@extends('dashboard::layouts.master')

@section('content')
    
    <h1 class="page-header">Logs</h1>

    <textarea readonly="readonly" name="logs" id="logs" class="form-control" rows="15">{{ $logs }}</textarea>
    
    @can('clear-logs')
    <br>
    <a href="{{ route('admin.logs.clear') }}" class="btn btn-primary">Clear Logs</a>
    @endcan
    
@stop