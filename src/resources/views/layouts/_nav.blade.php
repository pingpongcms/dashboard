<nav class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('admin.home') }}">{!! setting('dashboard_title', 'Admin') !!}</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="{{ route('home') }}" title="Visit Site" target="_blank"><i class="fa fa-external-link-square"></i></a></li>
            </ul>
            @include('menus::default', ['menu' => 'dashboard', 'class' => 'nav navbar-nav navbar-right'])
        </div><!-- /.navbar-collapse -->
    </div>
</nav>