<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ setting('dashboard_title') }}</title>

        <link rel="stylesheet" href="{{ asset('css/all.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin.css') }}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        @yield('style')
    </head>
    <body>
        @include('dashboard::layouts._nav')
        
        <div class="container">
            @yield('content')
        </div>
        
        <footer class="container">
            <hr>
            Copyright &copy; {{ date('Y') }}. All rights reserved.
        </footer>

        <script src="{{ asset('js/all.js') }}"></script>
        <script>
            var App = {
                token: '{{ csrf_token() }}',
                dashboardUrl: '{{ url(setting("dashboard_prefix", "admin")) }}',
            };
        </script>

        <script src="{{ asset('js/admin.js') }}"></script>
        @yield('script')
    </body>
</html>