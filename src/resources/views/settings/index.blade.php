@extends('dashboard::layouts.master')

@section('content')
    
    <h1 class="page-header">Settings</h1>

    {!! Form::open(['files' => true]) !!}

        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#general" aria-controls="general" role="tab" data-toggle="tab">General</a>
                </li>
                <li role="presentation">
                    <a href="#posts" aria-controls="posts" role="tab" data-toggle="tab">Posts</a>
                </li>
                <li role="presentation">
                    <a href="#dashboard" aria-controls="dashboard" role="tab" data-toggle="tab">Dashboard</a>
                </li>
                <li role="presentation">
                    <a href="#themes" aria-controls="themes" role="tab" data-toggle="tab">Themes</a>
                </li>
                <li role="presentation">
                    <a href="#auth" aria-controls="auth" role="tab" data-toggle="tab">Authentication</a>
                </li>
            </ul>
        
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="general">
                    @can('update-site_name')
                    <div class="form-group">
                        <label for="site_name">Site Name:</label>
                        {!! Form::text('settings[site_name]', setting('site_name'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-site_slogan')
                    <div class="form-group">
                        <label for="site_slogan">Site Slogan:</label>
                        {!! Form::text('settings[site_slogan]', setting('site_slogan'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-site_description')
                    <div class="form-group">
                        <label for="site_description">Site Description:</label>
                        {!! Form::textarea('settings[site_description]', setting('site_description'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-site_keywords')
                    <div class="form-group">
                        <label for="site_keywords">Site Keywords:</label>
                        {!! Form::text('settings[site_keywords]', setting('site_keywords'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-site_author')
                    <div class="form-group">
                        <label for="site_author">Site Author:</label>
                        {!! Form::text('settings[site_author]', setting('site_author'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                </div>
                <div role="tabpanel" class="tab-pane" id="posts">
                    @can('update-post_editor')
                    <div class="form-group">
                        <label for="post_editor">Post editor:</label>
                        {!! Form::select('settings[post_editor]', $postEditors, setting('post_editor'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-post_perpage')
                    <div class="form-group">
                        <label for="post_perpage">Number of Post PerPage:</label>
                        {!! Form::selectRange('settings[post_perpage]', 1, 50, setting('post_perpage'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-comment_perpage')
                    <div class="form-group">
                        <label for="comment_perpage">Number of Comment PerPage:</label>
                        {!! Form::selectRange('settings[comment_perpage]', 1, 50, setting('comment_perpage'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-comment_enabled')
                    <div class="form-group">
                        <label for="comment_enabled">Enable Comment:</label><br>
                        <label>
                            {!! Form::radio('settings[comment_enabled]', 1, setting('comment_enabled') == 1) !!}
                            Yes
                        </label>
                        <label>
                            {!! Form::radio('settings[comment_enabled]', 0, setting('comment_enabled') == 0) !!}
                            No
                        </label>
                    </div>
                    @endcan
                </div>
                <div role="tabpanel" class="tab-pane" id="dashboard">
                    @can('update-dashboard_title')
                    <div class="form-group">
                        <label for="dashboard_title">Dashboard Title:</label>
                        {!! Form::text('settings[dashboard_title]', setting('dashboard_title'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-dashboard_prefix')
                    <div class="form-group">
                        <label for="dashboard_prefix">Dashboard Prefix URL:</label>
                        {!! Form::text('settings[dashboard_prefix]', setting('dashboard_prefix'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-site_prefix')
                    <div class="form-group">
                        <label for="site_prefix">Site Prefix URL:</label>
                        {!! Form::text('settings[site_prefix]', setting('site_prefix'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                    @can('update-tracking_script')
                    <div class="form-group">
                        <label for="tracking_script">Tracking Script:</label>
                        {!! Form::textarea('settings[tracking_script]', setting('tracking_script'), ['class' => 'text-code form-control']) !!}
                        <p class="help-block">You can put your custom tracking script here. Such as Google Analytics (GA).</p>
                    </div>
                    @endcan
                </div>
                <div role="tabpanel" class="tab-pane" id="themes">
                    @can('update-theme_current')
                    <div class="form-group">
                        <label for="theme_current">Current Theme:</label>
                        {!! Form::select('settings[theme_current]', $themes, setting('theme_current'), ['class' => 'form-control']) !!}
                    </div>
                    @endcan
                </div>
                <div role="tabpanel" class="tab-pane" id="auth">
                   @can('update-register_enabled')
                    <div class="form-group">
                        <label for="register_enabled">Enable Registration:</label><br>
                        <label>
                            {!! Form::radio('settings[register_enabled]', 1, setting('register_enabled') == 1) !!}
                            Yes
                        </label>
                        <label>
                            {!! Form::radio('settings[register_enabled]', 0, setting('register_enabled') == 0) !!}
                            No
                        </label>
                    </div>
                    @endcan
                </div>
            </div>
        </div>

        <div class="form-group">
            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
        </div>

    {!! Form::close() !!}

@stop