{!! Form::open(['method' => 'GET', 'url' => isset($url) ? $url : null ]) !!}
    <div class="input-group">
        <input type="search" name="q" value="{{ request('q') }}" class="form-control" placeholder="Search...">
        @foreach (request()->query() as $name => $value)
            @if (!in_array($name, ['q']))
                {!! Form::hidden($name, $value) !!}
            @endif
        @endforeach
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit">
                <i class="glyphicon glyphicon-search"></i>
            </button>
        </span>
    </div><!-- /input-group -->
{!! Form::close() !!}