@extends('dashboard::layouts.master')

@section('content')
    
    <h1 class="page-header" style="margin-top: 10px;">Dashboard <small>Overview</small></h1>
    
    @if (Auth::user()->is('admin', 'super-admin'))
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row hero-unit">
                            <div class="col-lg-8">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                            <div class="col-lg-4">
                                <span class="hero-unit-count">{{ $users->count() }}</span>
                                Users
                            </div>
                        </div>
                    </div>
                    <div class="list-group">
                        <a href="{{ route('admin.users.index') }}" class="list-group-item">
                            View Details <i class="fa fa-arrow-circle-right pull-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row hero-unit">
                            <div class="col-lg-8">
                                <i class="fa fa-book fa-5x"></i>
                            </div>
                            <div class="col-lg-4">
                                <span class="hero-unit-count">{{ $posts->count() }}</span>
                                Posts
                            </div>
                        </div>
                    </div>
                    <div class="list-group">
                        <a href="{{ route('admin.posts.index') }}" class="list-group-item">
                            View Details <i class="fa fa-arrow-circle-right pull-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <div class="row hero-unit">
                            <div class="col-lg-8">
                                <i class="fa fa-flag fa-5x"></i>
                            </div>
                            <div class="col-lg-4">
                                <span class="hero-unit-count">{{ $pages->count() }}</span>
                                Pages
                            </div>
                        </div>
                    </div>
                    <div class="list-group">
                        <a href="{{ route('admin.posts.index', ['type' => 'page']) }}" class="list-group-item">
                            View Details <i class="fa fa-arrow-circle-right pull-right"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <div class="row hero-unit">
                            <div class="col-lg-8">
                                <i class="fa fa-comments fa-5x"></i>
                            </div>
                            <div class="col-lg-4">
                                <span class="hero-unit-count">{{ $comments->count() }}</span>
                                Comments
                            </div>
                        </div>
                    </div>
                    <div class="list-group">
                        <a href="{{ route('admin.comments.index') }}" class="list-group-item">
                            View Details <i class="fa fa-arrow-circle-right pull-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endif
    
@stop