<?php

namespace Pingpongcms\Dashboard\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class LogsController extends Controller
{
    private $logPath;

    public function __construct()
    {
        $this->logPath = storage_path('logs/laravel.log');
    }

    public function index()
    {
        $this->authorize('see-logs');

        $logs = File::exists($path = $this->logPath) ? File::get($path) : null;

        return view('dashboard::logs.index', compact('logs'));
    }

    public function clear()
    {
        $this->authorize('clear-logs');
        
        if (File::exists($path = $this->logPath)) {
            File::delete($path);
        }

        return redirect()->back();
    }
}
