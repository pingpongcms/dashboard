<?php

namespace Pingpongcms\Dashboard\Http\Controllers;

use App\Http\Controllers\Controller;
use Pingpongcms\Comments\Comment;
use Pingpongcms\Posts\Post;
use Pingpongcms\Terms\Term;
use Pingpongcms\Users\User;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::type('post')->get();

        $pages = Post::type('page')->get();
        
        $users = User::all();
        
        $comments = Comment::all();

        $tags = Term::tags()->get();

        $categories = Term::tags()->get();

        return view('dashboard::index', compact('posts', 'pages', 'comments', 'tags', 'categories', 'users'));
    }
}
