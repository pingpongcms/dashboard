<?php

Route::get('/', [
    'as' => 'home',
    'uses' => 'HomeController@index',
]);

Route::get('logs', [
    'uses' => 'LogsController@index',
    'as' => 'logs.index',
]);
Route::get('logs/clear', [
    'uses' => 'LogsController@clear',
    'as' => 'logs.clear',
]);