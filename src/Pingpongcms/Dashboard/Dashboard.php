<?php

namespace Pingpongcms\Dashboard;

use Closure;
use Illuminate\Support\Facades\Route;

class Dashboard
{
    public function routes(Closure $callback)
    {
        $attributes = [
            'as' => 'admin.',
            'prefix' => setting('dashboard_prefix', 'admin'),
            'middleware' => ['web', 'auth']
        ];

        Route::group($attributes, $callback);
    }
}