<?php

namespace Pingpongcms\Dashboard\Providers;

use Illuminate\Support\ServiceProvider;
use Pingpongcms\Dashboard\Dashboard;

class DashboardServiceProvider extends ServiceProvider
{
	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadTranslationsFrom(__DIR__ . '/../../../resources/lang', 'dashboard');

		$viewsSourcePath = __DIR__ . '/../../../resources/views';
		
		$viewsPath = base_path('resources/views/vendor/pingpongcms/dashboard');

		$this->loadViewsFrom([$viewsPath, $viewsSourcePath], 'dashboard');
		
		$this->publishes([$viewsSourcePath => $viewsPath], 'views');

		$this->publishes([
			__DIR__ . '/../../../../public' => public_path()
		], 'assets');

		$this->addSettingTabs();
	}

	private function addSettingTabs()
	{
		$this->app['settings.tabs']->add('Dashboard', function ($tab) {
		    $tab
		        ->addSetting('Dashboard Title:', 'dashboard_title', 'text')
		        ->addSetting('Dashboard Prefix URL:', 'dashboard_prefix', 'text')
		    ;
		})->order(5);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('dashboard', function ($app)
		{
			return new Dashboard;
		});
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return [];
	}

}
