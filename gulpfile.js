var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.assetsPath = 'assets';

var paths = {
    bower: '/../../bower_components'
}

elixir(function(mix) {
    mix.sass('app.scss')
        .less('admin.less', 'public/css/admin.css')
        .styles([
            paths.bower + '/bootstrap/dist/css/bootstrap.css',
            paths.bower + '/font-awesome/css/font-awesome.css',
            paths.bower + '/summernote/dist/summernote.css',
            paths.bower + '/summernote/dist/summernote-bs3.css',
            paths.bower + '/bootstrap-markdown/css/bootstrap-markdown.min.css',
        ])
        .scripts([
            paths.bower + '/jquery/dist/jquery.js',
            paths.bower + '/bootstrap/dist/js/bootstrap.js',
            paths.bower + '/summernote/dist/summernote.js',
            paths.bower + '/bootstrap-markdown/js/bootstrap-markdown.js',
            paths.bower + '/markdown/lib/markdown.js',
            paths.bower + '/vue/dist/vue.js',
            paths.bower + '/vue-resource/dist/vue-resource.js',
        ])
        .scripts('admin.js', 'public/js/admin.js')
        .scripts('post-form.js', 'public/js/dashboard/post-form.js')
        // sometimes this doesn't work :(
        // and you need to copy the fonts manually
        .copy(paths.bower + '/font-awesome/fonts/*', 'public/fonts')
        .copy(paths.bower + '/bootstrap/fonts/*', 'public/fonts')
    ;
});
