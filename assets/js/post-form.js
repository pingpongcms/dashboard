new Vue({
    el: '#postForm',
    data: {
        title: '',
        slug: ''
    },
    methods: {
        makeSlug: function () {
            var slug = this.title
                .toLowerCase()
                .replace(/[^\w ]+/g,'')
                .replace(/ +/g,'-')
            ;

            this.$set('slug', slug);
        }
    }
});